﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System.Diagnostics;
public class AppManager : MonoBehaviour
{
    public enum BlockMode { block,finance};
    public BlockMode blockMode;
    public List<Resident> allResidentList = new List<Resident>();
    public List<Room> allRoomsList = new List<Room>();

    /// <summary>
    /// Add Resident Parameters
    /// </summary>
   
    public InputField nameInputField, sureNameInputField, phoneInputField, emailInputField, paidInputField, yearInputField;
    public InputField roomCostInputField, roomDepositeInputField;
    public Button saveNewRoomButton;
    //public TMP_Dropdown blockDropDownList;
  //  public Button saveResident;
  
    public TextMeshProUGUI roomPriceText, roomTitleText, depostiteValue, roomPaidValue, roomRemainingValue;

    //Finance
    public TextMeshProUGUI totalBlockMoney, totalBlockPaid, totalBlockRemaining;
    //

    public int currentRoom;
    public GameObject currentOpenRoomPage;
    public string currentBlock;

    public List<GameObject> allBlockUIList;

    public GameObject roomUIGameObject;
    public GameObject roomTableContainer;
    public GameObject rowPrefab;

    public List<string> appMessagesList;
    public Color[] appMessageColors;
    [System.Serializable]
    public struct Pages
    {
        public GameObject dashboardPage;
        public GameObject blocksPage;
        public GameObject roomsPage;
        public GameObject roomDetailsPage;
        public GameObject financePage;
        public GameObject newResidentPage;
        public GameObject inAppMessagePage;
        public GameObject confirmDeleteMessagePage;
        public GameObject roomAddPage;

        public GameObject[] allPages;
        public GameObject[] allBlocks;
    }
    [SerializeField]
    public Pages pages;

    public GameObject deleteButton;
    public GameObject confirmDeleteButton;
    public Resident currentOpenedResident;

    public bool isEdit;
    public Color usedRoomColor, emptyRoomColor, otherRoomColor;
    public GameObject mainPageTable;
    public static AppManager Instance;

    [HideInInspector]
    public bool showSaveResAfterRoomAadd = false;
    private bool isNewRoom;
    public GameObject[] legendsGameObjects;

    public Button[] sidePanelButtons;
    public GameObject financeInfoContainer;
    // Start is called before the first frame update

    private void Awake()
    {
        CheckIfAppOpened();
    }
    void Start()
    {

        Instance = this;
        deleteButton.GetComponent<Button>().onClick.AddListener(() => ShowDeleteConfirmMessage());
        confirmDeleteButton.GetComponent<Button>().onClick.AddListener(() => DeleteResident());
        saveNewRoomButton.onClick.AddListener(() => AddNewRoom());
        ShowDashboard();
        Screen.SetResolution(1920, 1080, true);
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void ShowDashboard()
    {
        DisableAllPages();
        pages.dashboardPage.SetActive(true);
        mainPageTable.GetComponent<TableHandler>().ResetTable();

        IEnumerable<Resident> RES =
           from res in allResidentList
           where res.Year_2 == YearManagement.Instance.currentUsedYear
           select res;

        foreach (var item in RES)
        {
            //if(item.Year==YearManagement.Instance.currentUsedYear)
            mainPageTable.GetComponent<TableHandler>().AddRowMainPage(item);
        }
        SetButtonColor(sidePanelButtons[0]);
        GetComponent<ChartsHandler>().UpdateChart();
    }
    public void ShowBlocks()
    {
        DisableAllPages();
        pages.blocksPage.SetActive(true);
        SetButtonColor(sidePanelButtons[1]);
    }
    public void ShowBlocksRooms(string block)
    {
        foreach (var item in pages.allBlocks)
        {
            item.SetActive(false);
        }
        switch (block)
        {
            case "A":
                pages.allBlocks[0].SetActive(true);
                currentOpenRoomPage = pages.allBlocks[0];
                break;
            case "B":
                pages.allBlocks[1].SetActive(true);
                currentOpenRoomPage = pages.allBlocks[1];

                break;
            case "C":
                pages.allBlocks[2].SetActive(true);
                currentOpenRoomPage = pages.allBlocks[2];

                break;
            case "D":
                pages.allBlocks[3].SetActive(true);
                currentOpenRoomPage = pages.allBlocks[3];

                break;
            case "E":
                pages.allBlocks[4].SetActive(true);
                currentOpenRoomPage = pages.allBlocks[4];

                break;
            case "F":
                pages.allBlocks[5].SetActive(true);
                currentOpenRoomPage = pages.allBlocks[5];

                break;
            case "G":
                pages.allBlocks[6].SetActive(true);
                currentOpenRoomPage = pages.allBlocks[6];

                break;
            case "H":
                pages.allBlocks[7].SetActive(true);
                currentOpenRoomPage = pages.allBlocks[7];

                break;
            case "I":
                pages.allBlocks[8].SetActive(true);
                currentOpenRoomPage = pages.allBlocks[8];
                break;
            case "J":
                pages.allBlocks[9].SetActive(true);
                currentOpenRoomPage = pages.allBlocks[9];
                break;
            default:
                break;
        }
        currentBlock = block;
    }
    public void ShowFinance()
    {
        DisableAllPages();
        pages.financePage.SetActive(true);
        SetButtonColor(sidePanelButtons[2]);
        GetComponent<ChartsHandler>().UpdateFinanceChart(); 
    }
    public void ShowRoomDetail()
    {
        DisableAllPages();
        pages.roomDetailsPage.SetActive(true);
        DatabaseManager.Instance.GetAllResidents();
        FindObjectOfType<TableHandler>().ResetTable();
        AddRowsToTable(currentBlock, currentRoom);
        isEdit = false;
        Room room = GetRoom(currentBlock, currentRoom);
        roomPriceText.text = room != null ? room.Totalcost.ToString() : "0";
        roomTitleText.text = "Room " + currentBlock + "-" + currentRoom;
        depostiteValue.text = room != null ? room.Deposit.ToString() : "0";
        double remainigMoney = 0;
        if (room!=null)
        {
            remainigMoney = room.Totalcost+room.Deposit;
        }
        double roomAllPaid = 0;
        foreach (var item in allResidentList)
        {
            if (item.Year_2 == YearManagement.Instance.currentUsedYear)
            {
                if (item.Block == currentBlock && item.Room == currentRoom)
                {
                    roomAllPaid = item.Paid + roomAllPaid;
                     remainigMoney =remainigMoney- item.Paid;
                }
            }
        }
        roomRemainingValue.text = remainigMoney.ToString();
        roomPaidValue.text = roomAllPaid.ToString();
    }
    public double GetRoomPrice(string block,int room)
    {
        var roo = from re in allRoomsList
                          where re.RoomId == block + room && re.Year_2==YearManagement.Instance.currentUsedYear
                          select re;
        double val = roo.FirstOrDefault().Totalcost+ roo.FirstOrDefault().Deposit;
        UnityEngine.Debug.Log("Room=" + val);
        return val;
        

    }
    public double GetRoomPayments()
    {
       
        var residents = from res in allResidentList
                        where res.Room == currentRoom && res.Block == currentBlock && res.Year_2==YearManagement.Instance.currentUsedYear
                        select res;
        double totalpaid = 0;
        foreach (var item in residents)
        {
            totalpaid += item.Paid;
        }
        UnityEngine.Debug.Log("TOTAL=" + totalpaid);
        return totalpaid;
    }
    public void ShowSaveResident()
    {
        //ResetInpuData();
        showSaveResAfterRoomAadd = false;

        DatabaseManager.Instance.GetAllRooms();
        bool isItNewRoom = true;
        foreach (var item in allRoomsList)
        {
            if (item.Year_2 == YearManagement.Instance.currentUsedYear)
            {
                if (item.RoomId == currentBlock + currentRoom)
                {
                    isItNewRoom = false;
                }
            }

        }
        isNewRoom = isItNewRoom;
        if (isItNewRoom)
        {
            ShowNewRoomPage();
            showSaveResAfterRoomAadd = true;
            return;
        }
        else
        {
            DisableAllPages();
            pages.newResidentPage.SetActive(true);
        }
        if (isEdit)
        {
            deleteButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            deleteButton.GetComponent<Button>().interactable = false;

        }
    }
    public void ShowNewRoomPage()
    {
        Room currentR = GetRoom(currentBlock, currentRoom);
        if (currentR != null)
        {
            roomDepositeInputField.text = currentR.Deposit.ToString();
            roomCostInputField.text = currentR.Totalcost.ToString();
        }
        else
        {
            roomDepositeInputField.text = "0";
            roomCostInputField.text = "0";
        }
        pages.roomAddPage.SetActive(true);
    }
    public void ShowMessage(int messageID, int colorId)
    {
        Text tex = pages.inAppMessagePage.GetComponentInChildren<Text>();
        Image image = pages.inAppMessagePage.GetComponentsInChildren<Image>()[1];
        tex.text = appMessagesList[messageID];
        pages.inAppMessagePage.SetActive(true);
        image.color = appMessageColors[colorId];
    }
    public void ShowErrorMessage(string message)
    {

    }
    public void ShowDeleteConfirmMessage()
    {
        pages.confirmDeleteMessagePage.SetActive(true);
    }
    public void CloseErrorMessage()
    {
        pages.inAppMessagePage.SetActive(false);
    }
    public void CloseNewRoomPage()
    {
        pages.roomAddPage.SetActive(false);

    }
    public void CloseConfirmDeleteMessage()
    {
        pages.confirmDeleteMessagePage.SetActive(false);

    }
    public void DisableAllPages()
    {
        foreach (var item in pages.allPages)
        {
            item.SetActive(false);
        }
    }
    public Room GetRoom(string block, int num)
    {
        foreach (var item in allRoomsList)
        {
            if (item.Year_2 == YearManagement.Instance.currentUsedYear)
            {
                if (item.RoomId == block + num)
                {
                    return item;

                }
            }
        }
        return null;
    }

    public void AddRowsToTable(string block, int roomNum)
    {
        List<Resident> roomResidents = new List<Resident>();
        TableHandler tbHandler = FindObjectOfType<TableHandler>();

        foreach (var item in allResidentList)
        {
            if (item.Year_2 == YearManagement.Instance.currentUsedYear)
            {
                if (item.Block == block && item.Room == roomNum)
                {
                    roomResidents.Add(item);
                    tbHandler.AddRow(item);
                }
            }
        }

    }
    public void EditData(Resident oldres)
    {
        nameInputField.text = oldres.Name;
        sureNameInputField.text = oldres.Surname;
        phoneInputField.text = oldres.Phone;
        emailInputField.text = oldres.Email;
        paidInputField.text = oldres.Paid.ToString();
        ShowSaveResident();
    }
    public void ShowDeleteButton()
    {

    }
    public void CloseSaveResidentPage()
    {
      //  ResetInpuData();
        ShowRoomDetail();
    }
    public void ResetInpuData()
    {
        nameInputField.text = "";
        sureNameInputField.text = "";
        phoneInputField.text = "";
        emailInputField.text = "";
        paidInputField.text = "";
    }
    public void AddNewRoom()
    {
        isNewRoom = true;
        foreach (var item in allRoomsList)
        {
            if (item.Year_2 == YearManagement.Instance.currentUsedYear)
            {
                if (item.RoomId == currentBlock + currentRoom)
                {
                    isNewRoom = false;
                }
            }

        }
        if (isNewRoom)
        {
            DatabaseManager.Instance.AddRoom();
        }

        else
        {
            EditRoom();
            DatabaseManager.Instance.GetAllRooms();
            ShowRoomDetail();
        }
    }
    public void EditRoom()
    {
        Room room = new Room();
        room.RoomId = currentBlock + currentRoom;
        room.Totalcost = int.Parse(roomCostInputField.text);

        room.Deposit = int.Parse(roomDepositeInputField.text);

        DatabaseManager.Instance.ModifyRoom(room);
        UpdateRoomResidentsRemaining();
    }

    private void UpdateRoomResidentsRemaining()
    {
        var roomResidents = from res in allResidentList
                            where res.Room == currentRoom && res.Block == currentBlock && res.Year_2 == YearManagement.Instance.currentUsedYear
                            select res;
        var room = from roo in allRoomsList
                   where roo.RoomId == currentBlock + currentRoom && roo.Year_2 == YearManagement.Instance.currentUsedYear
                   select roo;
        foreach (var item in roomResidents)
        {
            item.Remainig = room.FirstOrDefault().Totalcost + room.FirstOrDefault().Deposit - GetRoomPayments();
            DatabaseManager.Instance.UpdateRoomRemainig(item);
        }
    }

    public void SaveResident()
    {
        if (!isEdit)
        {
            DatabaseManager.Instance.AddResident();
          //  ResetInpuData();
        }
        else
        {
            Resident newRes = new Resident();
            SaveEditData(currentOpenedResident, newRes);
        }
    }
    public void DeleteResident()
    {
        DatabaseManager.Instance.DeleteResident(currentOpenedResident);
    }
    public void SaveEditData(Resident oldRes, Resident newRes)
    {
        oldRes = currentOpenedResident;
        newRes.Name = nameInputField.text;
        newRes.Surname = sureNameInputField.text;
        newRes.Phone = phoneInputField.text == "" ? "0" : phoneInputField.text;
        newRes.Email = emailInputField.text == "" ? "" : emailInputField.text;
        newRes.Block = currentBlock;
        newRes.Paid = double.Parse(paidInputField.text == "" ? "0" : paidInputField.text);
        newRes.Remainig = GetRoomPrice(currentBlock, currentRoom)-GetRoomPayments();
        DatabaseManager.Instance.ModifyResident(oldRes, newRes);
        UpdateRoomResidentsRemaining();
    }

    public void SetButtonColor(Button bt)
    {
        foreach (var item in sidePanelButtons)
        {
            item.GetComponent<Image>().color = new Color(item.GetComponent<Image>().color.r, item.GetComponent<Image>().color.g, item.GetComponent<Image>().color.b, 1);
        }
        bt.GetComponent<Image>().color = new Color(bt.GetComponent<Image>().color.r, bt.GetComponent<Image>().color.g, bt.GetComponent<Image>().color.b, 0.5f);
    }
    public void QuitApp()
    {
        Application.Quit();
    }
    public void ToggleFullScreen()
    {
        bool screenState = Screen.fullScreen;
        if (screenState)
        {
            Screen.SetResolution(1024, 576, false);
            screenState = false;
        }
        else
        {
            Screen.SetResolution(1920, 1080, true);
            screenState = true;
        }
    }
    public void ShowLegend(int id)
    {
        foreach (var item in legendsGameObjects)
        {
            item.SetActive(false);
        }
        legendsGameObjects[id].SetActive(true);
    }
    public void CLoseAllLegends()
    {
        foreach (var item in legendsGameObjects)
        {
            item.SetActive(false);
        }
    }
    public void Opendb()
    {
        
        if (Process.GetProcessesByName("DB Browser for SQLCipher").Length > 0)
        {
            var process = Process.GetProcessesByName("DB Browser for SQLCipher");
            process[0].Kill();
            Process.Start(Application.streamingAssetsPath + "/Database/Aymu.db.sqbpro");

        }
        else
        {
            Process.Start(Application.streamingAssetsPath + "/Database/Aymu.db.sqbpro");
        }
        //or directly to open external programs:
       // System.Diagnostics.Process.Start("notepad.exe");
    }
    public void CheckIfAppOpened()
    {
        if (Process.GetProcessesByName(Application.productName).Length > 1)
        {
            Application.Quit();
        }
    }
    public void ShowFinanceInformation(double totalMoney,double totalPaid,double totalRemaining)
    {
        financeInfoContainer.SetActive(true);
        totalBlockMoney.text = totalMoney.ToString();
        totalBlockPaid.text = totalPaid.ToString();
        totalBlockRemaining.text = totalRemaining.ToString();
    }
       
}
