﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;
using System;
public class DatabaseManager : MonoBehaviour
{
    private string connectionString;

    public static DatabaseManager Instance;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        connectionString = "URI=file:" + Application.streamingAssetsPath + "/Database/Aymu.db";
        GetAllResidents();
        GetAllRooms();
    }
    public void GetAllResidents()
    {
        AppManager appManager = GetComponent<AppManager>();
        appManager.allResidentList.Clear();
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (IDbCommand dbCommand = dbConnection.CreateCommand())
            {
                string sqlQuery = "SELECT * from Residents   Order by Name";
                dbCommand.CommandText = sqlQuery;
                using (IDataReader reader = dbCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Resident newRes = new Resident();
                        newRes.Name = reader.GetString(0);
                        newRes.Surname = reader.GetString(1);
                        newRes.Phone = reader.IsDBNull(2) ? "" : reader.GetString(2);
                        newRes.Email = reader.IsDBNull(3) ? "" : reader.GetString(3);
                        newRes.Block = reader.IsDBNull(4) ? "" : reader.GetString(4);
                        newRes.Room = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                        newRes.Paid = reader.IsDBNull(6) ? 0 : reader.GetDouble(6);
                    //    newRes.Remaining = reader.IsDBNull(7) ? 0 : reader.GetDouble(7);

                        newRes.Remainig= reader.IsDBNull(7) ? 0 : reader.GetDouble(7);
                        newRes.Year_1 = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                        newRes.Year_2 = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);

                        // Debug.Log(newRes.Surname);
                        appManager.allResidentList.Add(newRes);


                    }
                    dbConnection.Close();
                    reader.Close();


                }
            }
        }
    }
    public void AddResident()
    {
        AppManager appManager = GetComponent<AppManager>();
        Resident res = new Resident();
        res.Name = appManager.nameInputField.text;
        res.Surname = appManager.sureNameInputField.text;
        res.Phone = appManager.phoneInputField.text == "" ? "0" : appManager.phoneInputField.text;
        res.Email = appManager.emailInputField.text == "" ? "" : appManager.emailInputField.text;
        res.Block = appManager.currentBlock;
      //  Debug.Log(res.Block);
        res.Room = appManager.currentRoom;
      
        res.Paid = double.Parse(appManager.paidInputField.text == "" ? "0" : appManager.paidInputField.text);
        res.Year_2 = YearManagement.Instance.currentUsedYear;
        res.Year_1 = res.Year_2 - 1;
        res.Remainig = appManager.GetRoomPrice(appManager.currentBlock,appManager.currentRoom)-appManager.GetRoomPayments();
        if (res.Name == "")
        {
            appManager.ShowMessage(0, 0);
            return;
        }
        if (res.Surname == "")
        {
            appManager.ShowMessage(1, 0);
            return;
        }
        
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
           // using (IDbCommand dbCommand = dbConnection.CreateCommand())
           try
            {
                IDbCommand dbCommand = dbConnection.CreateCommand();
                string sqlQuery = "INSERT INTO Residents (Name, Surname,Phone,Email,Block,Room,Paid,Year_2,Year_1,Remaining) VALUES ('" + res.Name + "', '" + res.Surname + "','" + res.Phone + "', '" + res.Email + "','" + res.Block + "', '" + res.Room + "','" + res.Paid + "', '" + res.Year_2 + "','" + res.Year_1 + "','" + res.Remainig+ "');";
                dbCommand.CommandText = sqlQuery;
                dbCommand.ExecuteNonQuery();
                dbConnection.Close();
                appManager.ShowMessage(2, 1);
                appManager.ShowRoomDetail();
            }
            catch (System.Exception e)
            {
                appManager.ShowMessage(3, 0);
                Debug.Log(e.Message);
            }
        }
    }

    public void ModifyResident(Resident oldRes, Resident res)
    {
        AppManager appManager = GetComponent<AppManager>();
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            // using (IDbCommand dbCommand = dbConnection.CreateCommand())
            try
            {
                IDbCommand dbCommand = dbConnection.CreateCommand();
                string sqlQuery = "UPDATE Residents Set Name='"+res.Name+"',Surname='"+ res.Surname+"',Phone='"+res.Phone+"',Email='"+res.Email+"',Paid='"+res.Paid+ "',Remaining='" + res.Remainig + "'Where Name='"+oldRes.Name+"' And Surname='"+oldRes.Surname+"' And Year_2='"+YearManagement.Instance.currentUsedYear+ "'";// VALUES ('" + res.Name + "', '" + res.Surname + "','" + res.Phone + "', '" + res.Email + "','" + res.Block + "', '" + res.Room + "','" + res.Paid + "', '" + res.Year + "');";
                dbCommand.CommandText = sqlQuery;
                dbCommand.ExecuteNonQuery();
                dbConnection.Close();
                appManager.ShowMessage(2, 1);
                appManager.ShowRoomDetail();
            }
            catch (System.Exception e)
            {
                appManager.ShowMessage(3, 0);
                Debug.Log(e.Message);
            }
        }
    }
    public void UpdateRoomRemainig(Resident res)
    {
        Debug.Log("S");
        AppManager appManager = GetComponent<AppManager>();
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            // using (IDbCommand dbCommand = dbConnection.CreateCommand())
            try
            {
                IDbCommand dbCommand = dbConnection.CreateCommand();
                string sqlQuery = "UPDATE Residents Set Remaining='"+res.Remainig+"'Where Name='"+res.Name+"' And Surname='"+res.Surname+"' And Year_2='"+YearManagement.Instance.currentUsedYear+ "'";// VALUES ('" + res.Name + "', '" + res.Surname + "','" + res.Phone + "', '" + res.Email + "','" + res.Block + "', '" + res.Room + "','" + res.Paid + "', '" + res.Year + "');";
                dbCommand.CommandText = sqlQuery;
                dbCommand.ExecuteNonQuery();
                dbConnection.Close();
                //appManager.ShowMessage(2, 1);
               // appManager.ShowRoomDetail();
            }
            catch (System.Exception e)
            {
                appManager.ShowMessage(3, 0);
                Debug.Log(e.Message);
            }
        }
    }

    public void DeleteResident(Resident res)
    {
        AppManager appManager = GetComponent<AppManager>();
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            // using (IDbCommand dbCommand = dbConnection.CreateCommand())
            try
            {
                IDbCommand dbCommand = dbConnection.CreateCommand();
                string sqlQuery = "DELETE From Residents where Name='"+res.Name+"' And Surname='"+res.Surname+ "' And Year_2='" + YearManagement.Instance.currentUsedYear + "'";
                dbCommand.CommandText = sqlQuery;
                dbCommand.ExecuteNonQuery();
                dbConnection.Close();
                appManager.ShowMessage(2, 1);
                appManager.ShowRoomDetail();
                appManager.CloseConfirmDeleteMessage();

            }
            catch (System.Exception e)
            {
                appManager.ShowMessage(3, 0);
                appManager.CloseConfirmDeleteMessage();
                Debug.Log(e.Message);
            }


        }
    }
    public void AddPayment(Resident res, float value)
    {

    }
    public void AddRoomPrice(float price)
    {

    }

    public void GetAllRooms()
    {
        AppManager appManager = GetComponent<AppManager>();
        appManager.allRoomsList.Clear();
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (IDbCommand dbCommand = dbConnection.CreateCommand())
            {
                string sqlQuery = "SELECT * from Rooms Order by RoomID";
                dbCommand.CommandText = sqlQuery;
                using (IDataReader reader = dbCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                  //      Debug.Log("SS" + reader.GetString(1));

                        Room newRoom = new Room();
                        newRoom.RoomId = reader.GetString(0);
                        newRoom.Totalcost =reader.GetInt32(1);
                        newRoom.Year_1 = reader.GetInt32(2);
                        newRoom.Year_2 = reader.GetInt32(3);
                        newRoom.Deposit = reader.GetInt32(4);



                        appManager.allRoomsList.Add(newRoom);


                    }
                    dbConnection.Close();
                    reader.Close();

                }
            }
        }
    }
    public void AddRoom()
    {
        AppManager appManager = GetComponent<AppManager>();
        Room room = new Room();
        room.RoomId = appManager.currentBlock + appManager.currentRoom;
        room.Totalcost = int.Parse(appManager.roomCostInputField.text == "" ? "0" : appManager.roomCostInputField.text);
        room.Deposit = int.Parse(appManager.roomDepositeInputField.text == "" ? "0" : appManager.roomDepositeInputField.text);
        room.Year_2 = YearManagement.Instance.currentUsedYear;
        room.Year_1 = room.Year_2 - 1;


        if (room.Totalcost == 0)
        {
            appManager.ShowMessage(4, 0);
            return;
        }
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            // using (IDbCommand dbCommand = dbConnection.CreateCommand())
            try
            {
                IDbCommand dbCommand = dbConnection.CreateCommand();
                string sqlQuery = "INSERT INTO Rooms (RoomID, TotalCost,Year_2,Year_1,Deposit) VALUES ('" + room.RoomId + "', '" + room.Totalcost + "','" + room.Year_2 + "','" + room.Year_1 + "','" + room.Deposit + "');";
                dbCommand.CommandText = sqlQuery;
                dbCommand.ExecuteNonQuery();
                dbConnection.Close();
                //appManager.ShowMessage(2, 1);
                if (appManager.showSaveResAfterRoomAadd)
                    appManager.ShowSaveResident();
                else
                {
                    GetAllRooms();
                    appManager.ShowRoomDetail();
                }
                Debug.Log("ROOM ADDED");
            }
            catch (System.Exception e)
            {
                appManager.ShowMessage(4, 0);
                Debug.Log(e.Message);
            }


        }
    }
    public void ModifyRoom(Room room)
    {
      ///  Debug.LogWarning("MODIFY");
        AppManager appManager = GetComponent<AppManager>();
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            // using (IDbCommand dbCommand = dbConnection.CreateCommand())
            try
            {
                IDbCommand dbCommand = dbConnection.CreateCommand();
                string sqlQuery = "UPDATE Rooms Set TotalCost='" + room.Totalcost+ "',Deposit='" + room.Deposit + "' Where RoomID='" + room.RoomId + "' And Year_2='" + YearManagement.Instance.currentUsedYear + "'";// VALUES ('" + res.Name + "', '" + res.Surname + "','" + res.Phone + "', '" + res.Email + "','" + res.Block + "', '" + res.Room + "','" + res.Paid + "', '" + res.Year + "');";
                dbCommand.CommandText = sqlQuery;
                dbCommand.ExecuteNonQuery();
                dbConnection.Close();
                appManager.ShowMessage(2, 1);
                appManager.ShowRoomDetail();
            }
            catch (System.Exception e)
            {
                appManager.ShowMessage(4, 0);
                Debug.Log(e.Message);
            }


        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}
