﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
[ExecuteInEditMode]
public class DropDownListEvents : MonoBehaviour
{
    private Dropdown dropdown;
    public List<UnityEvent> events;
    // Start is called before the first frame update
   
    void Start()
    {
        SetEvents();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetEvents()
    {
        dropdown = GetComponent<Dropdown>();

        foreach (var item in dropdown.options)
        {
            UnityEvent @event = new UnityEvent();
            events.Add(@event);
        }
    }
    public void RunEvent()
    {
       
    }
}
