﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class RowAttribute : MonoBehaviour
{
    public TextMeshProUGUI name, surname, phone, email, paid;
    // Start is called before the first frame update
    void Start()
    {
        TextMeshProUGUI[] includedTexts = GetComponentsInChildren<TextMeshProUGUI>();
        name = includedTexts[0];
        surname = includedTexts[1];
        phone = includedTexts[2];
        email = includedTexts[3];
        paid = includedTexts[4];
        GetComponent<Button>().onClick.AddListener(()=> AppManager.Instance.EditData(RowResident()));
    }
    public void SetRowValue(Resident resident)
    {
        name.text = resident.Name;
        surname.text = resident.Surname;
        phone.text = resident.Phone;
        email.text = resident.Email;
        paid.text = resident.Paid.ToString();

    }
    public Resident RowResident()
    {
        Resident res = new Resident();
        res.Name = name.text;
        res.Surname = surname.text;
        res.Phone = phone.text;
        res.Email = email.text;
        res.Paid = int.Parse(paid.text);
        AppManager.Instance.currentOpenedResident = res;
        AppManager.Instance.isEdit = true;
        return res;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
