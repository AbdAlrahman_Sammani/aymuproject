﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockApp : MonoBehaviour
{
    // Start is called before the first frame update
    public int day, month, year;
    public GameObject lockPanel;
    void Start()
    {
     DateTime date = new DateTime(year,month,day);
        if (DateTime.Now.Date > date)
        {
            lockPanel.SetActive(true);
        }
        lockPanel.GetComponentInChildren<Button>().onClick.AddListener(() => CloseApp());
        

    }

// Update is called once per frame
void Update()
    {
        
    }
    public void CloseApp()
    {
        Application.Quit();
    }
}
