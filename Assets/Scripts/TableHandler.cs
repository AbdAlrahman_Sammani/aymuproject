﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableHandler : MonoBehaviour
{
    public List<GameObject> rowsList;
    public GameObject rowPrefab;
    public GameObject rowsParent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void AddRow(Resident res)
    {
       GameObject newRow= Instantiate(rowPrefab,rowsParent.transform);
        rowsList.Add(newRow);
        newRow.GetComponent<RowAttribute>().SetRowValue(res);
    }

    public void AddRowMainPage(Resident res)
    {
        GameObject newRow = Instantiate(rowPrefab, rowsParent.transform);
        rowsList.Add(newRow);
        newRow.GetComponent<RowAttributeMainPage>().SetRowValue(res);
    }
    public void ResetTable()
    {
        foreach (var item in rowsList)
        {
            Destroy(item);
        }
        rowsList.Clear();
    }
}
