﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
public class FinanceBlock : MonoBehaviour
{
    public enum Block { A, B, C, D, E, F, G, H, I, J }
    public Block block;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => ShowBlock());

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ShowBlock()
    {
        AppManager.Instance.blockMode = AppManager.BlockMode.finance;
        AppManager.Instance.currentBlock = block.ToString();
     //   Debug.Log(AppManager.Instance.currentBlock);
        foreach (var item in AppManager.Instance.pages.allPages)
        {
            item.SetActive(false);
        }
        AppManager.Instance.DisableAllPages();
        AppManager.Instance.pages.roomsPage.SetActive(true);
        AppManager.Instance.ShowBlocksRooms(AppManager.Instance.currentBlock);
        SetBlockColorFinance();
        AppManager.Instance.ShowLegend(1);
        ManageInfo();
    }

    public void SetBlockColorFinance()
    {
        AppManager appmanager = AppManager.Instance;
        IEnumerable<Resident> resQuery = from res in appmanager.allResidentList
                                         where res.Block == AppManager.Instance.currentBlock && res.Year_2 == YearManagement.Instance.currentUsedYear
                                         select res;
        double totalPaid = 0;
        foreach (var item in resQuery)
        {
            totalPaid += item.Paid;
        }
        List<Button> allRoomsButton = new List<Button>();
        AppManager.Instance.currentOpenRoomPage.GetComponentsInChildren<Button>(allRoomsButton);
        string currentBlock = AppManager.Instance.currentBlock;
      //  Debug.Log("Current Block=" + currentBlock + "Page" + AppManager.Instance.currentOpenRoomPage.name);
        bool isused = false;
        foreach (var item in allRoomsButton)
        {
            RoomAttribute attr = item.GetComponent<RoomAttribute>();
            //      item.GetComponent<Image>().color = AppManager.Instance.usedRoomColor;
            int roomNum = attr.roomNum;
            IEnumerable<Resident> re = from res in appmanager.allResidentList
                                             where res.Block == AppManager.Instance.currentBlock && res.Year_2 == YearManagement.Instance.currentUsedYear && res.Room==roomNum
                                             select res;
            if (re.Count() > 0) {
                double totalPayment = 0;
                foreach (var roomResident in re)
                {
                    totalPayment += roomResident.Paid;
                }
                IEnumerable<Room> room = from ro in appmanager.allRoomsList
                                           where ro.RoomId == AppManager.Instance.currentBlock+attr.roomNum && ro.Year_2 == YearManagement.Instance.currentUsedYear
                                         select ro;
                foreach (var roo in room)
                {
                    if (totalPayment >= (roo.Totalcost+roo.Deposit))
                    {
                        item.gameObject.GetComponent<Image>().color = appmanager.emptyRoomColor;
                    }
                    else
                    {
                        item.gameObject.GetComponent<Image>().color = appmanager.usedRoomColor;

                    }

                }
            }
            else
            {
                item.gameObject.GetComponent<Image>().color =appmanager.otherRoomColor;
            }
        }
    }
    public void ManageInfo()
    {
        AppManager appmanager = AppManager.Instance;

        IEnumerable<Room> getBlock = from room in appmanager.allRoomsList where room.RoomId.Substring(0, 1) == appmanager.currentBlock && room.Year_2==YearManagement.Instance.currentUsedYear select room;
        IEnumerable<Resident> getResInBlock = from res in appmanager.allResidentList
                                              where res.Block == appmanager.currentBlock && res.Year_2 == YearManagement.Instance.currentUsedYear
                                              select res;
        double totalMoney = 0;
        double totalPaid = 0;
        double deposit = 0;
        foreach (var item in getBlock)
        {
            totalMoney += item.Totalcost;
            deposit += item.Deposit;
        }
        foreach (var item in getResInBlock)
        {
            totalPaid += item.Paid;
        }
        totalMoney +=  deposit;
        var totalRemain = totalMoney - totalPaid;
      //  Debug.Log(totalPaid);
        appmanager.ShowFinanceInformation(totalMoney, totalPaid, totalRemain);

     
    }
}
