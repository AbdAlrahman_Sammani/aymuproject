﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;
using UnityEngine.UI;
using System.Linq;
public class YearManagement : MonoBehaviour
{
    public int currentYear;
    public int currentUsedYear;
    public Dropdown yearsDropDown;
    public List<int> allYearsList;

    public static YearManagement Instance;
    public static int yearStartMonth_static = 5;
    public int yearStartMonthSetter = 5;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        currentYear = CurrentYearGenerator();
        yearsDropDown.onValueChanged.AddListener((value) => { SetCurrentUsedYear(); AppManager.Instance.ShowDashboard(); });
        AddYearsToDropList();
        currentUsedYear = CurrentYearGenerator();
        yearsDropDown.value = yearsDropDown.options.Count;
        SetCurrentUsedYear();
        AppManager.Instance.ShowDashboard();
        yearStartMonth_static = yearStartMonthSetter;
    }
    public void AddYearsToDropList()
    {
        GetAllYearsInDatabase();
        List<Dropdown.OptionData> dropdownItemsList = new List<Dropdown.OptionData>();
        foreach (var item in allYearsList)
        {
            string season = (item - 1).ToString() + "-" + item.ToString();
            Dropdown.OptionData optionData = new Dropdown.OptionData(season);
            dropdownItemsList.Add(optionData);
        }

        yearsDropDown.AddOptions(dropdownItemsList);
    }
    public static int CurrentYearGenerator()
    {
        DateTime dt = DateTime.Now;
        if (dt.Month < yearStartMonth_static)
        {
            return dt.Year;
        }
        else
        {
            return dt.Year + 1;
        }
    }
    public void GetAllYearsInDatabase()
    {
        foreach (var item in AppManager.Instance.allRoomsList)
        {
            if (!allYearsList.Contains(item.Year_2))
            {
                allYearsList.Add(item.Year_2);
            }
        }
        if (!allYearsList.Contains(currentYear))
        {
            allYearsList.Add(currentYear);
        }
        allYearsList.Sort();
    }
    public void SetCurrentUsedYear()
    {
        string current = yearsDropDown.options[yearsDropDown.value].text;
        current = current.Substring(current.IndexOf("-")+1, current.Length-1 - current.IndexOf("-"));

        currentUsedYear = int.Parse(current);
    }
    public void AddYearList()
    {

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
