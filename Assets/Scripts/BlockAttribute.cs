﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BlockAttribute : MonoBehaviour
{
    public enum Block { A, B, C, D, E, F, G, H, I, J }
    public Block block;
    public List<GameObject> allButtonsList;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => ShowBlock());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ShowBlock()
    {
        AppManager.Instance.blockMode = AppManager.BlockMode.block;
        AppManager.Instance.currentBlock = block.ToString();
        foreach (var item in AppManager.Instance.pages.allPages)
        {
            item.SetActive(false);
        }
        AppManager.Instance.DisableAllPages();
        AppManager.Instance.pages.roomsPage.SetActive(true);
        AppManager.Instance.ShowBlocksRooms(AppManager.Instance.currentBlock);
        SetBlockColor();
        AppManager.Instance.ShowLegend(0);
        AppManager.Instance.financeInfoContainer.SetActive(false);
    }
    public void SetBlockColor()
    {
        List<Button> allRoomsButton = new List<Button>();
        AppManager.Instance.currentOpenRoomPage.GetComponentsInChildren<Button>(allRoomsButton);
        string currentBlock = AppManager.Instance.currentBlock;
        foreach (var item in allRoomsButton)
        {
            RoomAttribute attr = item.GetComponent<RoomAttribute>();
            //      item.GetComponent<Image>().color = AppManager.Instance.usedRoomColor;
            bool isused = false;
            foreach (var res in AppManager.Instance.allResidentList)
            {
                if (res.Year_2 == YearManagement.Instance.currentUsedYear)
                {
                    if (attr.roomNum == res.Room && currentBlock == res.Block)
                    {
                        isused = true;
                     //   Debug.Log(res.Room + "..." + attr.roomNum + "....." + res.Block);
                        break;

                    }
                }
            }
            if (isused)
            {
                item.GetComponent<Image>().color = AppManager.Instance.usedRoomColor;

            }
            else
            {
                item.GetComponent<Image>().color = AppManager.Instance.emptyRoomColor;

            }
        }
    }
}
