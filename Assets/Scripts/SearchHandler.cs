﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class SearchHandler : MonoBehaviour
{
    public TMP_InputField searchInputField;
    public Button searchButton;
    public TableHandler tableResult;
    // Start is called before the first frame update
    void Start()
    {
        searchButton.onClick.AddListener(() => Search());
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Search()
    {
        tableResult.ResetTable();
        string searchText = searchInputField.text.ToLower();
        if (searchText != "")
        {
            foreach (var item in AppManager.Instance.allResidentList)
            {
                if (item.Year_2 == YearManagement.Instance.currentUsedYear)
                {
                    if (searchText == item.Name.ToLower())
                    {
                        tableResult.AddRowMainPage(item);
                    }
                    else if (searchText == item.Surname.ToLower())
                    {
                       

                        tableResult.AddRowMainPage(item);
                    }
                    else if (searchText == item.Phone.ToLower())
                    {
                

                        tableResult.AddRowMainPage(item);
                    }
                    else if (searchText == item.Room.ToString())
                    {
                      

                        tableResult.AddRowMainPage(item);
                    }
                    else if (searchText == item.Paid.ToString())
                    {
                     

                        tableResult.AddRowMainPage(item);
                    }
                    else if (searchText ==item.Email.ToLower())
                    {
                      

                        tableResult.AddRowMainPage(item);
                    }
                    else if (searchText == item.Block.ToLower())
                    {
                      

                        tableResult.AddRowMainPage(item);
                    }
                }
            }
        }
        else if (searchText == "")
        {
   

            AppManager.Instance.mainPageTable.GetComponent<TableHandler>().ResetTable();
            foreach (var res in AppManager.Instance.allResidentList)
            {
                if (res.Year_2 == YearManagement.Instance.currentUsedYear)
                    AppManager.Instance.mainPageTable.GetComponent<TableHandler>().AddRowMainPage(res);
            }
        }
    }

}

