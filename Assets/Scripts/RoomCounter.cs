﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
[ExecuteInEditMode]

public class RoomCounter : MonoBehaviour
{
    public int startwith = 101;
    public string block = "A";
    public string offset = "07";
    public int minus = 6;
    // Start is called before the first frame update
    void Start()
    {
        DOFUNC();
    }
    public void DOFUNC()
    {
        int i = startwith;
        foreach (var item in GetComponentsInChildren<Button>())
        {
            item.GetComponentInChildren<TextMeshProUGUI>().text = i.ToString();
            item.GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
            item.GetComponentInChildren<TextMeshProUGUI>().fontStyle = FontStyles.Bold;
            item.name =block+ "-" + i;
            if (!item.GetComponent<RoomAttribute>())
            {
                item.gameObject.AddComponent<RoomAttribute>();
            }

            item.GetComponent<RoomAttribute>().roomNum = i;
            i++;
            if (i.ToString().Substring(1) == offset)
            {
                i = i + 100 - minus;
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        DOFUNC();
    }
}
