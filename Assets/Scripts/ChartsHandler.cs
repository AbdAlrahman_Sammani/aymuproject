﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;
using ChartAndGraph;
public class ChartsHandler : MonoBehaviour
{
    public GameObject[] roomsContainer;
    public GameObject dashboardChart;
    public GameObject financeChart;
    // Start is called before the first frame update
  

    // Update is called once per frame
    void Update()
    {
        
    }
    public int GetRoomsCount()
    {
        int roomsCount = 0;
        foreach (var item in roomsContainer)
        {
            Button[] bt = item.GetComponentsInChildren<Button>();
            roomsCount += bt.Length;
        }
        return roomsCount;
    }

    public int GetUsedRoomCount()
    {
        var results = from p in AppManager.Instance.allResidentList
                      where p.Year_2 == YearManagement.Instance.currentUsedYear
                      group p.Room by p.Block into g
                      select new { block = g.Key, roomNum = g.ToList() };
        int count = 0;
        foreach (var item in results)
        {
        var lis=   item.roomNum.Distinct();
            count += lis.Count();
           
        }
        return count;   
    }
    public void UpdateChart()
    {
        int allroomsCount = GetRoomsCount();
        int usedCount = GetUsedRoomCount();
        CanvasPieChart pieChart = dashboardChart.GetComponent<CanvasPieChart>();
        pieChart.DataSource.SetValue("Kullanılan Oda", (double)usedCount);
        pieChart.DataSource.SetValue("Boş oda", (double)allroomsCount-usedCount);
    }

    public double GetTotalRoomsPrice()
    {
        double total = 0;
        var query = from room in AppManager.Instance.allRoomsList
                    where room.Year_2 == YearManagement.Instance.currentUsedYear
                    select room;
        foreach (var item in query)
        {
            total += item.Totalcost+item.Deposit;
        }
        return total;

    }
    public double GetTotalIncome()
    {
        double total = 0;
        var query = from res in AppManager.Instance.allResidentList
                    where res.Year_2 == YearManagement.Instance.currentUsedYear
                    select res.Paid;
        foreach (var item in query)
        {
            total += item;
        }
        return total;
    }
    public void UpdateFinanceChart()
    {
        double totalPrice = GetTotalRoomsPrice();
        double totalIncome = GetTotalIncome();
        CanvasPieChart pieChart = financeChart.GetComponent<CanvasPieChart>();
        pieChart.DataSource.SetValue("Toplam Odenen", (double)totalIncome);
        pieChart.DataSource.SetValue("Kalan Toplam", (double)totalPrice - totalIncome);
    }
}
