﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;
public class RoomAttribute : MonoBehaviour
{
  
    public int roomNum;
    
    public string block;
    public TextMeshProUGUI roomText;
    // Start is called before the first frame update
    void Start()
    {
        Button bt = GetComponent<Button>();
        bt.onClick.AddListener(() => ShowRoom());
        roomText = GetComponentInChildren<TextMeshProUGUI>();
        
    }
    private void OnEnable()
    {
        if(AppManager.Instance.blockMode==AppManager.BlockMode.finance)
        ShowRoomFinance();
        else
        {
            roomText = GetComponentInChildren<TextMeshProUGUI>();
            roomText.text = roomNum.ToString();
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void ShowRoom()
    {
        AppManager.Instance.currentRoom = roomNum;
        AppManager.Instance.ShowRoomDetail();
    }
    public void ShowRoomFinance()
    {
        roomText = GetComponentInChildren<TextMeshProUGUI>();
        AppManager appManager = AppManager.Instance;
        IEnumerable<Resident> GetRes = from res in appManager.allResidentList
                                       where res.Block == appManager.currentBlock && res.Room == roomNum && res.Year_2==YearManagement.Instance.currentUsedYear
                                       select res;
        IEnumerable<Room> GetRoom = from room in appManager.allRoomsList
                                    where room.RoomId == appManager.currentBlock + roomNum && room.Year_2 == YearManagement.Instance.currentUsedYear
                                    select room;
        double totalIncome = 0;
        double totalcost = 0;
        if (GetRoom.Count() > 0)
        {
            totalcost = GetRoom.First().Totalcost+ GetRoom.First().Deposit;
        }

       
        foreach (var item in GetRes)
        {
            totalIncome += item.Paid;
        }
        var remain = totalcost - totalIncome;
        
        roomText.text =roomNum+"\n"+remain+ "₺";

    }

}
