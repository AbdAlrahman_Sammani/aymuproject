﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resident
{
    public string Name { get; set; }
    public string Surname { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }
    public string Block { get; set; }
    public int Room { get; set; }
    public double Paid { get; set; }

    public int Year_1 { get; set; }

    public int Year_2 { get; set; }
    public double Remainig { get; set; }



    public Resident() { }
   
}
